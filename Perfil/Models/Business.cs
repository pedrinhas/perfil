﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Web.Script.Serialization;

namespace Perfil.Models
{
    public class Business
    {
        string myConnectionString = ConfigurationManager.ConnectionStrings["inscricoes2SIGACAD"].ConnectionString;

        public Business()
        {
        }

        #region Aluno

        public List<AlunoCurso> GetNomeAlunoCursoAluno(string numMec)
        {
            List<AlunoCurso> listData = new List<AlunoCurso>();

            string numMecIn = numMec;

            try
            {
                using (SqlConnection con = new SqlConnection(myConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "GetNomeAlunoCursoAluno2";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@numero", System.Data.SqlDbType.Int).Value = numMecIn;

                        con.Open();

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            AlunoCurso item = new AlunoCurso();

                            item.nome = Convert.ToString(reader["nome"]);
                            item.codCurso = Convert.ToString(reader["codCurso"]);
                            item.curso = Convert.ToString(reader["curso"]);
                            item.numero = Convert.ToInt32(reader["numero"]);
                            item.anoCurricular = Convert.ToInt32(reader["anoCurricular"]);
                            item.anoInscricao = Convert.ToInt32(reader["anoInscricao"]);

                            try
                            {
                                item.idMatricula = Convert.ToInt32(reader["idMatricula"]);
                            }
                            catch
                            {
                                return null; //NÃO EXISTE MATRICULA ASSOCIADA AO ALUNO. NÃO DEIXA FAZER LOGIN.
                            }

                            listData.Add(item);
                        }
                    }

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                //ErrorLogUTAD.ErrorLogClient erro = new ErrorLogUTAD.ErrorLogClient(ConfigurationManager.AppSettings["app"]);
                //erro.LogError(ex);
            }

            return listData;
        }


        public int GetIDMatricula(int numero, int codCurso)
        {
            try
            {
                return Convert.ToInt32(string.Format("{0}{1}", numero, codCurso.ToString("0000")));
            }
            catch (Exception ex)
            {
                //ErrorLogUTAD.ErrorLogClient erro = new ErrorLogUTAD.ErrorLogClient(ConfigurationManager.AppSettings["app"]);
                //erro.LogError(ex);

                return -1;
            }
        }

        #endregion

        #region Foto & Regime

        public Foto GetFoto(string numero)
        {

            Foto item = new Foto();

            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["inscricoes2SIGACAD"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_GetFoto";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@numero", System.Data.SqlDbType.Int).Value = numero;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            item.nomeFoto = Convert.ToString(reader["nomeFoto"]).Trim();
                            item.foto = (byte[])reader["foto"];
                            item.formato = item.nomeFoto.Split('.')[1].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorLogUTAD.ErrorLogClient erro = new ErrorLogUTAD.ErrorLogClient(ConfigurationManager.AppSettings["app"]);
                //erro.LogError(ex);
            }

            return item;
        }

        public String GetRegime(string numero)
        {

            String item = "";

            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["inscricoes2SIGACAD"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_GetRegime";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@numero", System.Data.SqlDbType.Int).Value = numero;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            item = Convert.ToString(reader["regime"]).Trim();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorLogUTAD.ErrorLogClient erro = new ErrorLogUTAD.ErrorLogClient(ConfigurationManager.AppSettings["app"]);
                //erro.LogError(ex);
            }

            return item;
        }

        #endregion

        public Funcionario getUtilizadorInfo(string utilizador)
        {
            Funcionario func = new Funcionario();

            try
            {
                string urlWS = string.Format("{0}{1}", ConfigurationManager.AppSettings["GetFuncionarios"], utilizador);
                WebClient wcWS = new WebClient();
                wcWS.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["wsUser"], ConfigurationManager.AppSettings["wsPass"]);
                string data = wcWS.DownloadString(urlWS).ToUTF8();
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                List<Funcionario> list = serializer.Deserialize<List<Funcionario>>(data);


                //remover
                if (list.Count == 0)
                {
                    if (utilizador == "pgoncalves")
                        func.nome = "Pedro Gonçalves";
                    else
                        func.nome = "não é possível carregar informações";
                }
                else
                    //remover - fim
                    func = list[0];

                func.email = utilizador + "@utad.pt";

            }
            catch (Exception ex)
            {
                //ErrorLogUTAD.ErrorLogClient erro = new ErrorLogUTAD.ErrorLogClient(ConfigurationManager.AppSettings["app"]);
                //erro.LogError(ex);
            }

            return func;
        }

    }

    public class Funcionario
    {
        public string categoria { get; set; }
        public string codEscola { get; set; }
        public string codHabilitacao { get; set; }
        public string codUO { get; set; }
        public string escola { get; set; }
        public string habilitacao { get; set; }
        public string login { get; set; }
        public string nome { get; set; }
        public string numMec { get; set; }
        public string siglaEscola { get; set; }
        public string unidadeOrganica { get; set; }
        public string email { get; set; }
    }

    public class AlunoCurso
    {
        public string nome { get; set; }
        public string codCurso { get; set; }
        public string curso { get; set; }
        public string idRamo { get; set; }
        public int numero { get; set; }
        public string datainscricao { get; set; }
        public int anoCurricular { get; set; }
        public int anoInscricao { get; set; }
        public int idMatricula { get; set; }
    }
    public class Foto
    {
        public string nomeFoto { get; set; }
        public byte[] foto { get; set; }
        public string formato { get; set; }
    }

}