﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Perfil.ViewModels
{
    public class DadosPessoaisViewModel
    {
        [Display(Name = "Visibilidade do Perfil")]
        public int Permissao { get; set; }

        [Display(Name = "Username")]
        public string Username { get; set; }

        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "Nome Personalizado")]
        public string NomePersonalizado { get; set; }

        [Display(Name = "Extensão")]
        [MaxLength(4, ErrorMessage = "A Extenção não pode ter mais de 4 caracteres.")]
        public string Extensao { get; set; }

        [Display(Name = "Visibilidade")]
        public int PermissaoExtensao { get; set; }

        [Display(Name = "Página Pessoal")]
        public string PaginaPessoal { get; set; }

        [Display(Name = "Visibilidade")]
        public int PermissaoPaginaPessoal { get; set; }

        [Display(Name = "Gabinete")]
        [MaxLength(20, ErrorMessage = "O Gabinete não pode ter mais de 20 caracteres.")]
        public string Gabinete { get; set; }

        [Display(Name = "Visibilidade")]
        public int PermissaoGabinete { get; set; }

        [Display(Name = "Ligações")]
        public List<Ligacao> Ligacoes { get; set;}

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Visibilidade")]
        public int PermissaoEmail { get; set; }

        public List<Perfil> Perfis { get; set; }

    }

    public class Ligacao
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Url { get; set; }
        public int Permissao { get; set; }
        public SelectList ddList { get; set; }

    }

    public class Perfil
    {
        public int ID { get; set; }
        public string CodigoTipo { get; set; }
        public string Tipo { get; set; }
        public string UnidadeOrganica { get; set; }
        public string Categoria { get; set; }
        public List<Flag> Flags { get; set; }
    }

    public class Flag
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public SelectList ddList { get; set; }
    }

}