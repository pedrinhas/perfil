﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Perfil.ViewModels
{
    public class PasswordViewModel
    {
        [Required]
        [Display(Name = "Password Atual")]
        [DataType(DataType.Password)]
        public string PasswordAtual { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Confirmar Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

    }
}