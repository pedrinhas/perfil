﻿using Perfil.App_Start;
using Perfil.Helpers;
using Perfil.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Perfil.Controllers
{
    public class PerfilController : BaseController
    {
        [NotAvailable]
        public ActionResult AlterarPassword()
        {
            return View();
        }

        [UTADLoggedIn]
        public ActionResult AlterarDadosPessoais()
        {
            try
            {
                DadosPessoaisViewModel model = RCU.GetUserInfo(Autenticacao.Utilizador);

                if (model == null) throw new Exception();

                Populate(model);

                return View(model);
            }
            catch
            {
                TempData["error"] = "Ocorreu um erro ao aceder aos seus dados. Por favor contacte os SIC.";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [UTADLoggedIn]
        public ActionResult AlterarDadosPessoais(DadosPessoaisViewModel model)
        {
            DadosPessoaisViewModel def = new DadosPessoaisViewModel();

            if (ModelState.IsValid)
            {
                bool result = RCU.SaveUserLigacoes(model);

                if (result)
                    TempData["success"] = "Dados gravados com sucesso.";
                else
                    TempData["error"] = "Ocorreu um erro ao gravar os dados.";
            }
            else
            {
                TempData["warning"] = "Existem erros no preenchimento do formulário.";
            }

            Populate(model);

            return View(model);
        }

        [UTADLoggedIn]
        public JsonResult GetNiveisAcesso()
        {
            return new JsonResult() { Data = RCU.GetNiveisAcesso(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private void Populate(DadosPessoaisViewModel o)
        {
            List<Ligacao> listData = RCU.GetTiposLigacao().OrderBy(l => l.ID).ToList();
            //if(o.Ligacoes != null) listData.RemoveAll(l => o.Ligacoes.Any(a => a.ID == l.ID));

            int[] tiposNaoMostrar = { 3, 12, 16 };
            listData.RemoveAll(l => tiposNaoMostrar.Contains(l.ID));

            ViewBag.ListaLigacoesField = new SelectList(listData, "ID", "Nome", 0);

            List<IRCUAssemblies.Utilizadores.Visibilidade> listNiveisAcesso = RCU.GetNiveisAcesso();

            ViewBag.PermissaoLista = listNiveisAcesso;

            ViewBag.Permissao = new SelectList(listNiveisAcesso, "ID", "Nome", o.Permissao);

            ViewBag.PermissaoEmail = new SelectList(listNiveisAcesso, "ID", "Nome", o.PermissaoEmail);
            ViewBag.PermissaoExtensao = new SelectList(listNiveisAcesso, "ID", "Nome", o.PermissaoExtensao);
            ViewBag.PermissaoGabinete = new SelectList(listNiveisAcesso, "ID", "Nome", o.PermissaoGabinete);
            ViewBag.PermissaoPaginaPessoal = new SelectList(listNiveisAcesso, "ID", "Nome", o.PermissaoPaginaPessoal);

            if(o.Perfis != null)
            {
                foreach(var perfil in o.Perfis)
                {
                    foreach(var flag in perfil.Flags)
                    {
                        flag.ddList = new SelectList(listNiveisAcesso, "ID", "Nome", flag.Value);
                    }
                }
            }            

            if (o.Ligacoes != null)
            {
                ViewBag.Ligacoes = new object[o.Ligacoes.Count];
                foreach (var c in o.Ligacoes)
                {
                    c.ddList = new SelectList(listNiveisAcesso, "ID", "Nome", c.Permissao);
                }
            }
        }

    }
}