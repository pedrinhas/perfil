﻿using System.Text;

namespace Perfil
{
    public static class Extensions
    {
        public static string ToUTF8(this string s)
        {
            var bytes = Encoding.Default.GetBytes(s);
            return Encoding.UTF8.GetString(bytes);
        }
    }
}