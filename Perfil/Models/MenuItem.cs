﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Perfil.Models
{
    public class MenuItem
    {
        public MenuItem()
        {
        }

        public string Toggle { get; set; }
        public string Descricao { get; set; }
        public string Url { get; set; }
        public List<SubMenuItem> lstSubMenu { get; set; }
    }
}