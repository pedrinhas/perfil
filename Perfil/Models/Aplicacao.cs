﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Perfil.Models
{
    public class Aplicacao
    {
        public Aplicacao()
        {
        }

        public string nome { get; set; }
        public string titulo { get; set; }
        public string urlApp { get; set; }
        public string descricao { get; set; }
    }
}