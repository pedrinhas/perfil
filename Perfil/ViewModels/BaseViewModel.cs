﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Perfil.Models;

namespace Perfil.ViewModels
{
    public class BaseViewModel
    {
        public IEnumerable<Aplicacao> Aplicacoes { get; set; }
        public IEnumerable<MenuItem> Menu { get; set; }
        public IEnumerable<Linguagem> Linguagens { get; set; }
    }
}