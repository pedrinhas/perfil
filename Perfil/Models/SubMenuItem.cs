﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Perfil.Models
{
    public class SubMenuItem
    {
        public SubMenuItem()
        {
        }

        public string DescricaoSecond { get; set; }
        public string UrlSecond { get; set; }
    }
}