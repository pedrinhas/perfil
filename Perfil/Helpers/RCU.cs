﻿using Perfil.App_Start;
using Perfil.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Perfil.Helpers
{
    public static class RCU
    {
        public const string CODIGO_PERFIL_DOCENTE = "01";

        private const int ID_PAGINAPESSOAL = 3;
        private const int ID_EXTENSAO = 12;
        private const int ID_EMAIL = 14;
        private const int ID_GABINETE = 16;

        public const string KEY_DSD = "VisibilidadeDadosDSD";
        public const string KEY_UO = "VisibilidadeUO";
        public const string KEY_Categoria = "VisibilidadeCat";
        public const string KEY_Email = "VisibilidadeEmail";

        public static string[] KEY_ORDER = { KEY_Categoria, KEY_UO, KEY_DSD };

        public static bool SaveUserLigacoes(DadosPessoaisViewModel item)
        {
            try
            {
                IRCUAssemblies.WebpagePerfil.ContactosWebsiteUser utilizador = new IRCUAssemblies.WebpagePerfil.ContactosWebsiteUser();

                utilizador.Username = item.Username;
                utilizador.NomeUtilizador = item.Nome;
                utilizador.NomePersonalizado = item.NomePersonalizado ?? "";
                utilizador.Contactos = new List<IRCUAssemblies.Contactos.Contacto>();
                utilizador.Visibilidade = new IRCUAssemblies.Utilizadores.Visibilidade() { ID = item.Permissao };

                utilizador.ID = Autenticacao.IDUtilizador;

                utilizador.Perfil = new List<IRCUAssemblies.WebpagePerfil.ContactosWebsitePerfil>();

                foreach(var p in item.Perfis)
                {
                    var flags = new List<IRCUAssemblies.WebpagePerfil.ContactosWebsiteFlag>();
                    foreach(var f in p.Flags)
                    {
                        flags.Add(new IRCUAssemblies.WebpagePerfil.ContactosWebsiteFlag()
                        {
                            Key = f.Key,
                            Value = f.Value
                        });
                    }

                    utilizador.Perfil.Add(new IRCUAssemblies.WebpagePerfil.ContactosWebsitePerfil()
                    {
                        ID = Autenticacao.Perfis[p.CodigoTipo],
                        Flags = flags
                    });
                }

                //extensao
                if(!string.IsNullOrWhiteSpace(item.Extensao))
                utilizador.Contactos.Add(new IRCUAssemblies.Contactos.Contacto()
                {
                    Tipo = new IRCUAssemblies.Contactos.Contacto.TipoContacto() { ID = ID_EXTENSAO },
                    Valor = item.Extensao,
                    Visibilidade = new IRCUAssemblies.Utilizadores.Visibilidade() { ID = item.PermissaoExtensao }
                });

                //pagina pessoal
                if(!string.IsNullOrWhiteSpace(item.PaginaPessoal))
                utilizador.Contactos.Add(new IRCUAssemblies.Contactos.Contacto()
                {
                    Tipo = new IRCUAssemblies.Contactos.Contacto.TipoContacto() { ID = ID_PAGINAPESSOAL },
                    Valor = item.PaginaPessoal,
                    Visibilidade = new IRCUAssemblies.Utilizadores.Visibilidade() { ID = item.PermissaoPaginaPessoal }
                });

                //gabinete
                if (!string.IsNullOrWhiteSpace(item.Gabinete))
                utilizador.Contactos.Add(new IRCUAssemblies.Contactos.Contacto()
                {
                    Tipo = new IRCUAssemblies.Contactos.Contacto.TipoContacto() { ID = ID_GABINETE },
                    Valor = item.Gabinete,
                    Visibilidade = new IRCUAssemblies.Utilizadores.Visibilidade() { ID = item.PermissaoGabinete }
                });

                //email
                utilizador.Flags = new List<IRCUAssemblies.WebpagePerfil.ContactosWebsiteFlag>();
                utilizador.Flags.Add(new IRCUAssemblies.WebpagePerfil.ContactosWebsiteFlag()
                {
                    Key = KEY_Email,
                    Value = item.PermissaoEmail.ToString()                    
                });
                
                //if (!string.IsNullOrWhiteSpace(item.Email))
                //utilizador.Contactos.Add(new IRCUAssemblies.Contactos.Contacto()
                //{
                //    Tipo = new IRCUAssemblies.Contactos.Contacto.TipoContacto() { ID = ID_EMAIL },
                //    Valor = item.Email,
                //    Visibilidade = new IRCUAssemblies.Utilizadores.Visibilidade() { ID = item.PermissaoEmail }
                //});

                if (item.Ligacoes != null)
                foreach (var ligacao in item.Ligacoes)
                {
                    IRCUAssemblies.Contactos.Contacto c = new IRCUAssemblies.Contactos.Contacto()
                    {
                        Tipo = new IRCUAssemblies.Contactos.Contacto.TipoContacto()
                        {
                            ID = ligacao.ID
                        },
                        Valor = ligacao.Url,
                        Visibilidade = new IRCUAssemblies.Utilizadores.Visibilidade() {  ID = ligacao.Permissao }
                    };

                    utilizador.Contactos.Add(c);
                }

                return SaveUserContactos(utilizador);
            }
            catch(Exception e)
            {
                return false;
            }
        }

        private static bool SaveUserContactos(IRCUAssemblies.WebpagePerfil.ContactosWebsiteUser item)
        {
            string urlWS = ConfigurationManager.AppSettings["RCU.SaveContactos"].ToString();
            bool result = false;

            using (WebClient wcWS = new WebClient())
            {

                string wsusername = ConfigurationManager.AppSettings["RCU.WsUsername"].ToString(); ;
                string wspassword = ConfigurationManager.AppSettings["RCU.WsPassword"].ToString(); ;

                wcWS.Headers.Add(HttpRequestHeader.Authorization, string.Format("basic {0}", Convert.ToBase64String(Encoding.ASCII.GetBytes(wsusername + ":" + wspassword))));

                string data = "";

                JavaScriptSerializer s = new JavaScriptSerializer();
                string go = s.Serialize(item);

                wcWS.Encoding = Encoding.UTF8;

                wcWS.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                data = wcWS.UploadString(urlWS, WebRequestMethods.Http.Post, go);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                result = serializer.Deserialize<bool>(data);
            }

            return result;
        }

        public static DadosPessoaisViewModel GetUserInfo(string username)
        {
            try
            {
                DadosPessoaisViewModel utilizador = new DadosPessoaisViewModel();

                var userRCU = GetUserContactos(username);

                List<Ligacao> listData = new List<Ligacao>();

                foreach (var item in userRCU.Contactos)
                {
                    switch (item.Tipo.ID)
                    {
                        //extensao
                        case ID_EXTENSAO:
                            utilizador.Extensao = item.Valor;
                            utilizador.PermissaoExtensao = item.Visibilidade.ID;
                            break;
                        //pagina pessoal
                        case ID_PAGINAPESSOAL:
                            utilizador.PaginaPessoal = item.Valor;
                            utilizador.PermissaoPaginaPessoal = item.Visibilidade.ID;
                            break;
                        //gabinete
                        case ID_GABINETE:
                            utilizador.Gabinete = item.Valor;
                            utilizador.PermissaoGabinete = item.Visibilidade.ID;
                            break;
                        //email
                        //case ID_EMAIL:
                        //    utilizador.Email = item.Valor;
                        //    utilizador.PermissaoEmail = item.Visibilidade.ID;
                        //    break;
                        default:
                            listData.Add(new Ligacao() { ID = item.Tipo.ID, Nome = item.Tipo.Nome, Url = item.Valor, Permissao = item.Visibilidade.ID });
                            break;
                    }
                }

                utilizador.Ligacoes = listData;
                utilizador.Username = username;
                utilizador.Nome = userRCU.NomeUtilizador;
                utilizador.NomePersonalizado = userRCU.NomePersonalizado;
                utilizador.Permissao = userRCU.Visibilidade.ID;

                utilizador.Email = userRCU.Email;
                utilizador.PermissaoEmail = Convert.ToInt32(userRCU.Flags.Single(f => f.Key == KEY_Email).Value);

                Autenticacao.IDUtilizador = userRCU.ID;

                utilizador.Perfis = new List<ViewModels.Perfil>();

                Autenticacao.Perfis = new Dictionary<string, int>();

                foreach (var perfil in userRCU.Perfil)
                {
                    if(perfil.CodigoTipo == null) perfil.CodigoTipo = "00";
                    if(perfil.NomeTipo == null) perfil.NomeTipo = "";

                    Autenticacao.Perfis.Add(perfil.CodigoTipo, perfil.ID);

                    var p = new ViewModels.Perfil()
                    {
                        ID = perfil.ID,
                        CodigoTipo = perfil.CodigoTipo,
                        Tipo = perfil.NomeTipo,
                        Categoria = perfil.Categoria,
                        UnidadeOrganica = perfil.UnidadeOrganica,
                        Flags = new List<Flag>()
                    };

                    foreach (var flag in perfil.Flags)
                    {
                        p.Flags.Add(new Flag()
                        {
                            Key = flag.Key,
                            Value = flag.Value
                        });
                    }

                    p.Flags = p.Flags.OrderBy(f => Array.IndexOf(KEY_ORDER, f.Key)).ToList();

                    utilizador.Perfis.Add(p);
                }

                return utilizador;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        private static IRCUAssemblies.WebpagePerfil.ContactosWebsiteUser GetUserContactos(string username)
        {
            string urlWS = ConfigurationManager.AppSettings["RCU.UserContactos"].ToString();

            urlWS = string.Format(urlWS, username);

            using (WebClient wcWS = new WebClient())
            {
                string wsusername = ConfigurationManager.AppSettings["RCU.WsUsername"].ToString(); ;
                string wspassword = ConfigurationManager.AppSettings["RCU.WsPassword"].ToString(); ;

                wcWS.Headers.Add(HttpRequestHeader.Authorization, string.Format("basic {0}", Convert.ToBase64String(Encoding.ASCII.GetBytes(wsusername + ":" + wspassword))));

                string data = wcWS.DownloadString(urlWS).ToUTF8();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                
                IRCUAssemblies.WebpagePerfil.ContactosWebsiteUser utilizador = serializer.Deserialize<IRCUAssemblies.WebpagePerfil.ContactosWebsiteUser>(data);

                return utilizador;
            }
        }


        public static List<Ligacao> GetTiposLigacao()
        {
            try
            {
                var tiposContactos = GetTiposContactos();

                List<Ligacao> listData = new List<Ligacao>()
                {
                    new Ligacao()
                    {
                        ID = 0,
                        Nome = "Seleccione uma opção"
                    }
                };

                foreach (var item in tiposContactos)
                {
                    listData.Add(new Ligacao() { ID = item.ID, Nome = item.Nome });
                }

                return listData;
            }
            catch
            {
                return null;
            }
        }

        private static List<IRCUAssemblies.Contactos.Contacto.TipoContacto> GetTiposContactos()
        {
            string urlWS = ConfigurationManager.AppSettings["RCU.TiposContacto"].ToString();

            using (WebClient wcWS = new WebClient())
            {
                string wsusername = ConfigurationManager.AppSettings["RCU.WsUsername"].ToString(); ;
                string wspassword = ConfigurationManager.AppSettings["RCU.WsPassword"].ToString(); ;

                wcWS.Headers.Add(HttpRequestHeader.Authorization, string.Format("basic {0}", Convert.ToBase64String(Encoding.ASCII.GetBytes(wsusername + ":" + wspassword))));
                string data = wcWS.DownloadString(urlWS).ToUTF8();
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                List<IRCUAssemblies.Contactos.Contacto.TipoContacto> contactos = serializer.Deserialize<List<IRCUAssemblies.Contactos.Contacto.TipoContacto>>(data);

                return contactos;
            }
        }

        public static List<IRCUAssemblies.Utilizadores.Visibilidade> GetNiveisAcesso()
        {
            string urlWS = ConfigurationManager.AppSettings["RCU.GetNiveisAcesso"].ToString();

            using (WebClient wcWS = new WebClient())
            {
                string wsusername = ConfigurationManager.AppSettings["RCU.WsUsername"].ToString(); ;
                string wspassword = ConfigurationManager.AppSettings["RCU.WsPassword"].ToString(); ;

                wcWS.Headers.Add(HttpRequestHeader.Authorization, string.Format("basic {0}", Convert.ToBase64String(Encoding.ASCII.GetBytes(wsusername + ":" + wspassword))));
                string data = wcWS.DownloadString(urlWS).ToUTF8();
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                List<IRCUAssemblies.Utilizadores.Visibilidade> niveis_acesso = serializer.Deserialize<List<IRCUAssemblies.Utilizadores.Visibilidade>>(data);

                return niveis_acesso;
            }
        }

    }
}