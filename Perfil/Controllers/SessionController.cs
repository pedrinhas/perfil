﻿using System.Web.Mvc;
using Perfil.App_Start;
using System.Configuration;

namespace Perfil.Controllers
{
    public class SessionController : BaseController
    {
        private bool ext = false;

        [UTADLoggedOff]
        public ActionResult Login()
        {
            return View();
        }

        //POST: LoginUTAD/Login 
        [HttpPost]
        [UTADLoggedOff]
        public ActionResult Login(string username, string password)
        {
            Autenticacao.Logout();

            //VERIFY IF INPUTS ARE NOT EMPTY
            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {

                //VERIFY IF USER IS AN ADMINISTRATOR, AND SIMULATE USER
                if (Autenticacao.Authorization(username) && Autenticacao.Administrator(username, password) != null)
                {
                    Autenticacao.Utilizador = Autenticacao.Administrator(username, password);

                    Autenticacao.NomeUtilizador = "Administrador: " + username;
                }
                else
                {
                    TempData["error"] = "Não possui privilégios de administração!";
                }
            }

            if (TempData.ContainsKey("error"))
            {
                return RedirectToAction("Login", "Session");
            }
            else
            {
                if (Autenticacao.GetGrupo == "aluno")
                {
                    if (Autenticacao.LoadAlunoInfo())
                        return RedirectToAction("Index", "Home");
                    else
                    {
                        Autenticacao.Logout();
                        TempData["error"] = "Aluno não existe na base de dados intermédia!";
                        return RedirectToAction("Login", "Session");
                    }
                }
                /*caso a aplicação permita iniciar sessão com conta de funcionário, remover seguinte pedaço de código*/
                else
                {
                    Autenticacao.Logout();
                    TempData["error"] = "Apenas pode iniciar sessão com conta de aluno!";
                    return RedirectToAction("Login", "Session");
                }
            }

        }

        [UTADLoggedIn]
        public ActionResult Logout()
        {
            if (Session["shib"] == null)
            {
                Autenticacao.Logout();
                return Redirect(Url.Content("~/"));
            }
            else
            {
                Autenticacao.Logout();
                string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
                return Redirect(ConfigurationManager.AppSettings["ShibbolethRedirectUrl"]);
            }
        }

    }
}