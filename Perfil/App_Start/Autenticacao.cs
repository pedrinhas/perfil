﻿using System;
using System.Linq;
using System.DirectoryServices;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Perfil.Models;
using System.Collections.Generic;

namespace Perfil.App_Start
{
    public class Autenticacao
    {
        #region Private Session

        public static string Utilizador
        {
            get
            {
                return _Utilizador; //.ToString();
            }
            set
            {
                //int numeroUtilizador = Convert.ToInt32(System.Text.RegularExpressions.Regex.Match(value, @"\d+").Value);
                //_Utilizador = numeroUtilizador;
                _Utilizador = value;
            }
        }

        public static string NomeUtilizador
        {
            get
            {
                return _NomeUtilizador;
            }
            set
            {
                _NomeUtilizador = value;
            }
        }

        public static String UtilizadorField1
        {
            get
            {
                return _UtilizadorField1;
            }
            set
            {
                _UtilizadorField1 = value;
            }
        }

        public static String UtilizadorField2
        {
            get
            {
                return _UtilizadorField2;
            }
            set
            {
                _UtilizadorField2 = value;
            }
        }

        public static String CodigoCurso
        {
            get
            {
                return _CodigoCurso.ToString();
            }
            set
            {
                int codigoCurso = Convert.ToInt32(value);
                _CodigoCurso = codigoCurso;
            }
        }

        public static byte[] Foto
        {
            get
            {
                return _Foto;
            }
            set
            {
                _Foto = value;
            }
        }

        public static String FotoFormato
        {
            get
            {
                return _FotoFormato;
            }
            set
            {
                _FotoFormato = value;
            }
        }

        public static String Regime
        {
            get
            {
                return _Regime;
            }
            set
            {
                _Regime = value;
            }
        }

        public static int IDUtilizador
        {
            get
            {
                return _IDUtilizador;
            }
            set
            {
                _IDUtilizador = value;
            }
        }

        public static Dictionary<string, int> Perfis
        {
            get
            {
                return _Perfis;
            }
            set
            {
                _Perfis = value;
            }
        }

        private static string _Utilizador
        {
            get
            {
                if (HttpContext.Current.Session["Utilizador"] == null)
                    return null;
                else
                    return HttpContext.Current.Session["Utilizador"].ToString(); //Convert.ToInt32("0" + HttpContext.Current.Session["Utilizador"].ToString());
            }
            set
            {
                HttpContext.Current.Session.Add("Utilizador", value);
            }
        }

        private static string _NomeUtilizador
        {
            get
            {
                if (HttpContext.Current.Session["NomeUtilizador"] == null)
                    return null;
                else
                    return HttpContext.Current.Session["NomeUtilizador"].ToString();
            }
            set
            {
                HttpContext.Current.Session.Add("NomeUtilizador", value);
            }
        }

        private static String _UtilizadorField1
        {
            get
            {
                return HttpContext.Current.Session["UtilizadorField1"].ToString();
            }
            set
            {
                HttpContext.Current.Session.Add("UtilizadorField1", value);
            }
        }

        private static String _UtilizadorField2
        {
            get
            {
                return HttpContext.Current.Session["UtilizadorField2"].ToString();
            }
            set
            {
                HttpContext.Current.Session.Add("UtilizadorField2", value);
            }
        }
        private static int _CodigoCurso
        {
            get
            {
                if (HttpContext.Current.Session["CodigoCurso"] == null)
                    return 0;
                else
                    return Convert.ToInt32("0" + HttpContext.Current.Session["CodigoCurso"].ToString());
            }
            set
            {
                HttpContext.Current.Session.Add("CodigoCurso", value);
            }
        }
        private static byte[] _Foto
        {
            get
            {
                return (byte[])HttpContext.Current.Session["Foto"];
            }
            set
            {
                HttpContext.Current.Session.Add("Foto", value);
            }
        }
        private static string _FotoFormato
        {

            get
            {
                return HttpContext.Current.Session["FotoFormato"].ToString();
            }
            set
            {
                HttpContext.Current.Session.Add("FotoFormato", value);
            }
        }
        private static string _Regime
        {

            get
            {
                return HttpContext.Current.Session["Regime"].ToString();
            }
            set
            {
                HttpContext.Current.Session.Add("Regime", value);
            }
        }

        private static int _IDUtilizador
        {

            get
            {
                return Convert.ToInt32(HttpContext.Current.Session["IDUtilizador"]);
            }
            set
            {
                HttpContext.Current.Session.Add("IDUtilizador", value);
            }
        }

        private static Dictionary<string, int> _Perfis
        {

            get
            {
                return (Dictionary<string, int>)HttpContext.Current.Session["Perfis"];
            }
            set
            {
                HttpContext.Current.Session.Add("Perfis", value);
            }
        }

        #endregion Private Session

        private static string ldapKey = ConfigurationManager.AppSettings["ldapKey"].ToString();

        private static string authorizationList = ConfigurationManager.AppSettings["authorizationList"].ToString();

        private static string authorizationAdminList = ConfigurationManager.AppSettings["authorizationAdminList"].ToString();

        /*ADMINISTRATOR LOGIN WITH OTHER USER*/
        public static string Administrator(string username, string password)
        {

            if (username.Contains(";"))
            {
                string[] users = username.Split(';');

                foreach (string user in users)
                {

                    string adminUser = users[0].ToString();
                    string userSimulation = users[1].ToString();
                    
                    Boolean AuthenticationBoolResult = Authentication(adminUser, password);

                    if (AuthenticationBoolResult)
                    {
                        return userSimulation;
                    }

                }
            }
            return null;
        }

        /*VERIFY IF USER CAN ACCESS TO WEBSITE*/
        public static bool Authorization(string username)
        {
            username = username.Split(new char[] { ';' }).First();

            int numMec = 0;
            var users = authorizationList.Split(',').ToList();

            return username.StartsWith("al") && int.TryParse(username.Replace("al", ""), out numMec) || users.Contains(username) || users.Contains("all");

        }

        /*SEARCH USERNAME IN LDAP DIRECTORY*/
        public static bool Authentication(string username, string password)
        {

            //UID FOR ALUNOS
            String uid = "uid=" + username + ",OU=alunos,OU=organizacao,DC=utad,DC=pt";

            //ROOT FOR ALUNOS
            DirectoryEntry root = new DirectoryEntry(ldapKey, uid, password, AuthenticationTypes.None);

            try
            {
                //ATTEMPT TO USE LDAP CONNECTION WITH ALUNOS ROOT
                object connected = root.NativeObject;
                //NO EXCEPTION, LOGIN SUCCESFUL WITH ALUNOS ROOT              
                return true;
            }
            catch (Exception)
            {
                //UID FOR FUNCIONARIOS
                String uidFuncionarios = "uid=" + username + ",OU=pessoas,OU=organizacao,DC=utad,DC=pt";

                //ROOT FOR FUNCIONARIOS
                DirectoryEntry root2 = new DirectoryEntry(ldapKey, uidFuncionarios, password, AuthenticationTypes.None);

                try
                {
                    //ATTEMPT TO USE LDAP CONNECTION WITH FUNCIONARIOS ROOT
                    object connected = root2.NativeObject;
                    //NO EXCEPTION, LOGIN SUCCESFUL WITH FUNCIOINARIOS ROOT                 
                    return true;
                }
                catch (Exception)
                {
                    //EXCEPTION THROW, LOGIN FAILED                
                    return false;
                }

            }
        }

        /*GET THE CURRENT USER FULLNAME FROM LDAP DIRECTORY*/
        public static SearchResult LdapSearch(string username, string password)
        {

            try
            {
                //SEARCH IN ALUNOS DIRECTORY
                DirectoryEntry dea = new DirectoryEntry(ldapKey + "/OU=alunos,OU=organizacao,DC=utad,DC=pt", "UID=" + username + ",OU=alunos,OU=organizacao,DC=utad,DC=pt", password, AuthenticationTypes.None);

                //FILTER BY UID
                DirectorySearcher dsa = new DirectorySearcher(dea, "(UID=" + username + ")");
                SearchResult result = dsa.FindOne();

                return result;

            }
            catch
            {
                //SEARCH IN FUNCIONARIOS DIRECTORY
                DirectoryEntry def = new DirectoryEntry(ldapKey + "/OU=pessoas,OU=organizacao,DC=utad,DC=pt", "UID=" + username + ",OU=pessoas,OU=organizacao,DC=utad,DC=pt", password, AuthenticationTypes.None);

                //FILTER BY UID
                DirectorySearcher dsf = new DirectorySearcher(def, "(UID=" + username + ")");
                SearchResult result = dsf.FindOne();

                return result;
            }
        }


        /*--------------------------------------------------*/

        public static bool LoadAlunoInfo(string username = null)
        {
            if (username == null)
                username = Utilizador;
            else
                Utilizador = username;

            username = System.Text.RegularExpressions.Regex.Match(Utilizador, @"\d+").Value;

            Business x = new Business();

            try
            {
                AlunoCurso alunoInfo = x.GetNomeAlunoCursoAluno(username).First();
                Foto alunoFoto = x.GetFoto(username);
                string regime = x.GetRegime(username);

                CodigoCurso = alunoInfo.codCurso;
                Foto = alunoFoto.foto;
                FotoFormato = alunoFoto.formato;
                Regime = regime;

                NomeUtilizador = alunoInfo.nome;
                //ADD ALUNO ATTRIBUTES

                return true;
            }
            catch
            {
                Logout();
                return false;
            }

        }

        public static bool LoadFuncionarioInfo(string username = null)
        {
            if (username == null)
                username = Utilizador;
            else
                Utilizador = username;

            Business x = new Business();

            try
            {
                Funcionario funcionarioInfo = x.getUtilizadorInfo(username);

                NomeUtilizador = funcionarioInfo.nome;
                //ADD FUNCIONARIO ATTRIBUTES

                return true;
            }
            catch
            {
                Logout();
                return false;
            }

        }

        public static void Logout()
        {
            HttpContext.Current.Session.Clear();
        }

        public static bool IsAnonimo
        {
            get
            {
                return GetGrupo == "anonimo";
            }
        }

        public static bool IsAluno
        {
            get
            {
                return GetGrupo == "aluno";
            }
        }

        public static bool IsFuncionario
        {
            get
            {
                return GetGrupo == "funcionario";
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                if (_Utilizador != null)
                    return true;

                if (HttpContext.Current.Session["shib"] != null)
                {
                    string username = HttpContext.Current.Session["shib"].ToString();

                    //login aluno!!!!
                    string resultString = System.Text.RegularExpressions.Regex.Match(username, @"\d+").Value;

                    if (resultString != "")
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["autenticacaoAluno"]) && LoadAlunoInfo(resultString))
                            return true;
                        else
                            HttpContext.Current.Session["erroAutenticacao"] = "Não tem permissões de acesso.";
                    }
                    else
                    {
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["autenticacaoFuncionario"]) && Authorization(username) && LoadFuncionarioInfo(username))
                            return true;
                        else
                            HttpContext.Current.Session["erroAutenticacao"] = "Não tem permissões de acesso.";
                    }
                }

                return false;
            }
        }

        public static bool IsAdmin
        {
            get
            {
                var users = authorizationAdminList.Split(',').ToList();

                return users.Contains(Utilizador) || users.Contains("all");
            }
        }

        public static string GetGrupo
        {
            get
            {
                if (!IsLoggedIn)
                    return "anonimo";
                else
                {
                    if (System.Text.RegularExpressions.Regex.Match(Utilizador, @"\d+").Value != "")
                        return "aluno";
                    else
                        return "funcionario";
                }
            }
        }
    }

    public class UTADAdminAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return Autenticacao.IsAdmin;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["warning"] = "Tentativa de acesso inválido. Não tem permissão para aceder a esta funcionalidade.";

            filterContext.Result = new RedirectResult("~/Secure/Index");
        }
    }

    public class UTADLoggedInAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return Autenticacao.IsLoggedIn;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Session["returnURL"] = filterContext.HttpContext.Request.FilePath;

            filterContext.Result = new RedirectResult("~/Secure/Index");
        }
    }

    public class UTADLoggedOffAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return !Autenticacao.IsLoggedIn;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["warning"] = "Já se encontra com sessão iniciada.";

            filterContext.Result = new RedirectResult("~/Home/Index");
        }
    }


    public class NotAvailableAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["warning"] = "Esta operação não se encontra disponível.";

            filterContext.Result = new RedirectResult("~/Home/Index");
        }

    }
}